
/*
 * Copyright 2025 ailurux <ailuruxx@gmail.com>
 *
 * SPDX-License-Identifier: GPL-3.0-only
 */

#pragma once

namespace input {

enum class InputEvent {
    kNone = 0,
    kOnPress = 1,
    kOnLongPress = 2,
};

}  // namespace input